#!/bin/bash
#based on https://gist.github.com/mokagio/b974620ee8dcf5c0671f

# Only run if the tools are not installed yet
# To check that try to print the SDK path
xcode-select -p &> /dev/null
if [ $? -ne 0 ]; then
  touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;
  PROD=$(softwareupdate -l |
    grep "\*.*Command Line" |
    head -n 1 | awk -F"*" '{print $2}' |
    sed -e 's/^ *//' |
    sed 's/Label: //'|
    tr -d '\n')
  softwareupdate -i "$PROD";
else
  echo "Xcode CLI tools OK"
fi