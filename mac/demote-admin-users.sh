#!/bin/bash

if [ $# != 2 ]
then
{
	echo "$0 adminusertokeep passwordtoset"
	exit 1
}
fi
adminuser="$1"
temppasswd="$2"
adminuid="999"

###########################################
# function to print error message
exitmessage () {
if [ $? -eq 0 ]
then
	printf "" #placeholder for when not debugging
#   echo "Successfully ran $1"
else
	printf "" #placeholder for when not debugging
#   echo "$1 failed"
fi
}
###########################################
# function to list users in group
# "members admin"
members () {
  dscl . -list /Users | while read -r user; do
    printf "$user "
    dsmemberutil checkmembership -U "$user" -G "$*"
  done | grep "is a member" | cut -d " " -f 1;
};

###########################################
# seek vacant UID above $adminuid
# no useful /etc/password, so run "id" on UID to see if it's in use
# "no such user" is sent to stderr, so have to 
# turn off stdin and find a good ID https://unix.stackexchange.com/a/184807
# don't need stdin, so that goes to /dev/null too
exec 3>&2
exec 2> /dev/null
while id "$adminuid" >/dev/null ; do
	((adminuid--))
 	echo "changing UID to $adminuid"
done
#turn back on stderr
exec 2>/dev/tty

###########################################
# create adnmin user if does not exist

if [ "$(id "$adminuser" 2> /dev/null)" = "" ] ; then
	echo "creating $adminuser"
	sudo dscl . -create "/Users/$adminuser" && exitmessage "create" || exit 1
	echo "set shell"
	sudo dscl . -create "/Users/$adminuser" UserShell /bin/bash && exitmessage "UserShell" || exit 1
	echo "set name"
	sudo dscl . -create "/Users/$adminuser" RealName "TLOS Administrators" && exitmessage "RealName" || exit 1
	echo "setting ID = $adminuid"
	sudo dscl . -create "/Users/$adminuser" UniqueID $adminuid && exitmessage "UniqueID" || exit 1
	echo "set primary group"
	sudo dscl . -create "/Users/$adminuser" PrimaryGroupID 20 && exitmessage "PrimaryGroupID" || exit 1
	echo "set nfshome"
	sudo dscl . -create "/Users/$adminuser" NFSHomeDirectory "/Users/$adminuser" && exitmessage "NFSHomeDirectory"|| exit 1
	sudo mkdir "/Users/$adminuser" && exitmessage "mkdir" || exit 1
	sudo chown -R "$adminuser": "/Users/$adminuser" && exitmessage "chown" || exit 1
	sudo dscl . -append /Groups/admin GroupMembership "$adminuser" && exitmessage "GroupMembership" || exit 1
	sudo dscl . -passwd "/Users/$adminuser" "$temppasswd" && exitmessage "set temppasswd"||exit 1
fi

###########################################
# demote extra admin users
for val in $(members admin)
do
 	currentuser=$(whoami)
	# removes extra admins
	if [[ ! ($val == "tlosadmin" || $val == "root" ) ]]
	then
		# sudo fdesetup remove -user $val # <- removes filevault user
		sudo dseditgroup -o edit -d "$val" -t user admin
	fi
done