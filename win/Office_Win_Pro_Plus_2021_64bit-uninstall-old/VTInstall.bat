@echo off
cd /d "%~dp0"
:check_permissions
	net session >nul 2>&1
	if %errorLevel% ==0 (
		REM no message if admin privileges are detected
	) else (
		echo Please run this script with administrative privileges.
		echo Press any key to exit
		pause >nul
		goto END
	)

:install64
echo Installing Microsoft Office LTSC Professional Plus 2021 64-bit
start /wait /min setup.exe /configure Office2021-64bit.xml
cls

:end